// var myApp = angular.module('myApp',[]);
//
// myApp.controller('MyController', function($scope) {
//   $scope.spices = [{"name":"pasilla", "spiciness":"mild"},
//                    {"name":"jalapeno", "spiciness":"hot hot hot!"},
//                    {"name":"habanero", "spiciness":"LAVA HOT!!"}];
//   $scope.spice = "habanero";
// });

angular.module('heroApp', []);

function HeroDetailController() {
    var ctrl = this;

    ctrl.delete = function() {
        ctrl.onDelete({hero: ctrl.hero});
    };

    ctrl.update = function(prop, value) {
        ctrl.onUpdate({hero: ctrl.hero, prop: prop, value: value});
    };
}

angular.module('heroApp').component('heroDetail', {
    templateUrl: 'heroDetail.html',
    controller: HeroDetailController,
    bindings: {
        hero: '<',
        onDelete: '&',
        onUpdate: '&'
    }
});